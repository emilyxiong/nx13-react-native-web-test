import i18n, { setI18nConfig, initI18n, translate } from './lib/i18n'

export { setI18nConfig, initI18n, translate }
export default i18n
