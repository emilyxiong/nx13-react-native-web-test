// web/webpack.config.js
const webpack = require('webpack');
const path = require('path');

const appDirectory = path.resolve(__dirname, './');
const workspaceRootDirectory = path.resolve(__dirname, '../../');

// This is needed for webpack to compile JavaScript.
// Many OSS React Native packages are not compiled to ES5 before being
// published. If you depend on uncompiled packages they may cause webpack build
// errors. To fix this webpack can be configured to compile to the necessary
// `node_module`.

const compileNodeModules = [
  // Add every react-native package that needs compiling
  'react-native-localize',
  // 'react-router-native',
  // 'aws-amplify',
  // 'aws-amplify-react-native',
  // 'amazon-cognito-identity-js',
  // '@react-native-community/netinfo',
  // '@react-native-async-storage/async-storage',
].map((moduleName) =>
  path.resolve(workspaceRootDirectory, `node_modules/${moduleName}`)
);

const babelLoaderConfiguration = {
  test: /\.(js|jsx|ts|tsx)$/,
  // Add every directory that needs to be compiled by Babel during the build.
  include: [
    path.resolve(appDirectory, 'src/main.tsx'),
    path.resolve(appDirectory, 'src'),
    ...compileNodeModules,
  ],
  use: {
    loader: 'babel-loader',
    options: {
      cacheDirectory: true,
      // The 'metro-react-native-babel-preset' preset is recommended to match React Native's packager
      presets: ['module:metro-react-native-babel-preset'],
      // Re-write paths to import only the modules needed by the app
      plugins: ['react-native-web'],
    },
  },
};

// This is needed for webpack to import static images in JavaScript files.
const imageLoaderConfiguration = {
  test: /\.(gif|jpe?g|png|svg)$/,
  use: {
    loader: 'url-loader',
    options: {
      name: '[name].[ext]',
      esModule: false,
    },
  },
};

// require the main @nrwl/react/plugins/webpack configuration function.
const nrwlConfig = require('@nrwl/react/plugins/webpack.js');

module.exports = (config, context) => {
  nrwlConfig(config); // first call it so that it @nrwl/react plugin adds its configs,

  const result = {
    ...config,
    plugins: [
      ...config.plugins,
      //   new HtmlWebpackPlugin({
      //     template: path.join(appDirectory, 'src/index.html'),
      //   }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(
          process.env.NODE_ENV || 'development'
        ),
        __DEV__: process.env.NODE_ENV !== 'production' || true,
      }),
    ],
    node: { global: true },
    resolve: {
      ...config.resolve,
      alias: {
        ...config.resolve.alias,
        'react-native$': 'react-native-web',
      },
      extensions: [
        '.web.mjs',
        '.mjs',
        '.web.js',
        '.js',
        '.web.ts',
        '.ts',
        '.web.tsx',
        '.tsx',
        '.json',
        '.web.jsx',
        '.jsx',
      ],
    },
    module: {
      ...config.module,
      rules: [
        ...config.module.rules,
        babelLoaderConfiguration,
        imageLoaderConfiguration,
      ],
    },
  };
  return result;
};
