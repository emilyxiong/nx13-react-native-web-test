import React, { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { UiHeading } from '@acme/ui-heading';

import * as RNLocalize from 'react-native-localize';
import { setI18nConfig, initI18n, translate } from '@acme/shared/util/i18n';

initI18n();

export function App() {
  useEffect(() => {
    initI18n();
  }, []);

  useEffect(() => {
    const handleLocalizationChange = (): void => {
      setI18nConfig();
    };
    RNLocalize.addEventListener('change', handleLocalizationChange);
    return function cleanup(): void {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    };
  });
  return (
    <View style={styles.box}>
      <UiHeading text={translate('pages.welcome.welcome-message')} />
      <Text style={styles.text}>This is a demo page.</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  box: { padding: 10 },
  text: { fontSize: 18 },
});

export default App;
